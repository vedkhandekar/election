<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Candidate.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionDatabase.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';
    
    $db_handle = new mysqli('172.17.0.2', 'root', 'root'); //TODO: change while making Dockerfile
        
    $election_registry = new ElectionRegistry($db_handle);
    $election = $election_registry->get_election($_POST['voter_election_choice']);
    //$election_database = new ElectionDatabase($election, $db_handle);
    
    $candidates = $election->get_candidates();
    
    $symbolpath_r = array();
    foreach ($candidates as $candidate) {
        $symbolpath_r[$candidate->get_name()] = '/assets/candidate_symbols/' . basename($candidate->get_symbol_path());
    }
    
} else {
    header('Location: /index.php');
}

?>
