<?php

session_start();

if ($_SESSION['logged_in'] && $_SERVER['REQUEST_METHOD'] == 'POST') {
    
    /*require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Candidate.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Election.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionDatabase.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';

    $db_handle = new mysqli('172.17.0.2', 'root', 'root');
    $election_registry = new ElectionRegistry($db_handle);
    $stopped_elections = $election_registry->get_stopped_elections();*/
    
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Candidate.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Election.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionDatabase.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';

    $db_handle = new mysqli('172.17.0.2', 'root', 'root'); // TODO: change this while making docker file
    $election_registry = new ElectionRegistry($db_handle);
    $stopped_elections = $election_registry->get_stopped_elections();
    
    $admin_election_choice = $_POST['admin_election_choice'];
    $admin_election = $election_registry->get_election($admin_election_choice);
    $election_database = new ElectionDatabase($admin_election, $db_handle);
    
    $candidates = $election_database->get_election()->get_candidates_desc();
    
    $total_votes = 0;
    foreach ($candidates as $candidate) {
        $total_votes += $candidate->get_num_votes();
    }
    
    //var_dump($candidates);
            
} else if ($_SESSION['logged_in']) {

    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Candidate.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Election.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionDatabase.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';

    $db_handle = new mysqli('172.17.0.2', 'root', 'root');
    $election_registry = new ElectionRegistry($db_handle);
    $stopped_elections = $election_registry->get_stopped_elections();
    
    /*require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Candidate.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Election.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionDatabase.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';

    $db_handle = new mysqli('172.17.0.2', 'root', 'root');
    $election_registry = new ElectionRegistry($db_handle);
    $stopped_elections = $election_registry->get_stopped_elections();
    
    $admin_election_choice = $_POST['admin_election_choice'];
    $election_database = new ElectionDatabase($admin_election_choice, $db_handle);
    
    $candidates = $election_database->get_election()->get_candidates();
    
    var_dump($candidates);*/
    
} else {
    header('Location: /admin.php');
}
?>
