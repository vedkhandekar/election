<?php

session_start();

if (!$_SESSION['logged_in']) {
    header('Location: /admin.php');
}

require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Election.php';
require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';

$db_handle = new mysqli('172.17.0.2', 'root', 'root'); //TODO: change this while making Dockerfile
$election_registry = new ElectionRegistry($db_handle);

$started_elections = $election_registry->get_started_elections();
$stopped_elections = $election_registry->get_stopped_elections();

if (count($stopped_elections) > 0) {
    $stopped_elections_exist = TRUE;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //$election_registry->update_elections();
    $election = $election_registry->get_election($_POST['admin_choice']);
    $election->set_started(FALSE);
    $election_registry->set_election($_POST['admin_choice'], $election);
    $election_registry->update_registry();
    
    $started_elections = $election_registry->get_started_elections();
    $stopped_elections = $election_registry->get_stopped_elections();
    
    if (count($stopped_elections) > 0) {
        $stopped_elections_exist = TRUE;
    }

}

$db_handle->close();

?>
