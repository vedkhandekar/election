<?php

session_start();

if ($_SERVER['REQUEST_METHOD'] == 'GET' && !$_SESSION['logged_in']) {
    header('Location: /admin.php');
} else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $_SESSION['logged_in'] = TRUE;
}

?>
