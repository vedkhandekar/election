<?php

session_start();

if (!$_SESSION['logged_in']) {
    header('Location: /admin.php');
}

define('SYMBOLS_DIR', $_SERVER['DOCUMENT_ROOT'] . '/assets/candidate_symbols/');

$file_names = array_diff(scandir(SYMBOLS_DIR), array('..', '.'));

if ($_SESSION['logged_in'] && $_SERVER['REQUEST_METHOD'] == 'POST') {

    if (count($file_names) != 0) {
        foreach ($_POST as $file_index => $file_name) {
            if (strpos($file_index, 'file') !== FALSE) {
                unlink(SYMBOLS_DIR . $file_name);
            }
        }
    }
    
    $file_names = array_diff(scandir(SYMBOLS_DIR), array('..', '.'));
}
?>
