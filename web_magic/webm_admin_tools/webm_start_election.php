<?php
session_start();

if (!$_SESSION['logged_in']) {
    $db_handle->close();
    header('Location: /admin.php');
}

require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Election.php';
require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';

$db_handle = new mysqli('172.17.0.2', 'root', 'root'); //TODO: change this while making Dockerfile
$election_registry = new ElectionRegistry($db_handle);

if (count($election_registry->get_started_elections()) > 0) {
    $started_elections_exist = TRUE;
    $stopped_elections = $election_registry->get_stopped_elections();
    $started_elections = $election_registry->get_started_elections();
} else {
    $stopped_elections = $election_registry->get_stopped_elections();
}

if (count($election_registry->get_stopped_elections()) == 0) {
        $all_elections_active = TRUE;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $election = $election_registry->get_election($_POST['admin_choice']);
    $election->set_started(TRUE);
    $election_registry->set_election($_POST['admin_choice'], $election);
    $election_registry->update_registry();
    
    $started_elections_exist = TRUE;
    $stopped_elections = $election_registry->get_stopped_elections();
    $started_elections = $election_registry->get_started_elections();
    
    if (count($stopped_elections) == 0) {
        $all_elections_active = TRUE;
    }
    
} /*else if ($_SESSION['logged_in']) {
    
} else {
    header('Location: /admin.php');
}*/
$db_handle->close();
?>
