<?php

session_start();

if (!$_SESSION['logged_in']) {
    header('Location: /admin.php');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_SESSION['logged_in']) {
    
    $old_passwd = $_POST['old_passwd'];
    $new_passwd = $_POST['new_passwd'];    
    $cnf_new_passwd = $_POST['cnf_new_passwd'];

    if ($new_passwd != $cnf_new_passwd) {
        $msg = 'Passwords do not match! Please enter them again';
    } else {

        require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/AdminPassword.php';
        
        $db_handle = new mysqli('172.17.0.2', 'root', 'root');
        
        $admin_password = new AdminPassword($db_handle);
        
        if(password_verify($old_passwd, $admin_password->get_hash())) {
            $admin_password->write_hash(password_hash($new_passwd, PASSWORD_DEFAULT));
            $msg = 'Password changed successfully. Use the new password to login the next time';
        } else {
            $msg = 'Old password is incorrect';
        }
    }
}

?>
