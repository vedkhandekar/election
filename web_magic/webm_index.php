<?php
require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionDatabase.php';
require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';

$db_handle = new mysqli('172.17.0.2', 'root', 'root');
$election_registry = new ElectionRegistry($db_handle);

$started_elections = $election_registry->get_started_elections();
$started_elections_exist = count($started_elections) > 0 ? TRUE : FALSE;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    $voter_name = htmlspecialchars(trim($_POST['voter_name'])); //TODO: also trim in between spaces
    $voter_election_choice = $_POST['voter_election_choice'];
    
    $election_database = new ElectionDatabase($election_registry->get_election($voter_election_choice), $db_handle);
    
    if (str_word_count($voter_name) != 3) {
        $js_code = 'alert(\'Please enter your full name\');';
    } else if ($election_database->has_voted($voter_name)) {
        $js_code = sprintf('alert(\'%s, you have already voted in %s\')', $voter_name, $voter_election_choice);
    } else {
        $election_database->add_voter($voter_name);
        $action_location = 'vote_screen.php';
        $js_code = 'document.forms[0].submit()';
    }   
}
?>
