<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Election.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionDatabase.php';

class ElectionRegistry {
    private $elections = array();
    private $db_handle;
    
    function __construct($db_handle) {
        $this->set_db_handle($db_handle);
        
        if (!$this->election_registry_exists()) {
            $this->create_election_registry();    
        }
        
        $this->update_elections();
    }
    
    function create_election_registry() {
        $query = 'CREATE DATABASE election_registry;';
        $query .= 'CREATE TABLE election_registry.created_elections (tag VARCHAR(255) NOT NULL, started BOOLEAN NOT NULL);';
        $query .= 'CREATE TABLE election_registry.admin_passwd (passwd_hash VARCHAR(255) NOT NULL);';
        $query .= 'INSERT INTO election_registry.admin_passwd VALUES (\'$2y$10$/4DHRlUl2t2E13C9UlFDP.IqP8YKxcc.Z9DmV0en6FrowRsTO9EvO\')';
        
        
        foreach (explode(';', $query) as $line) {
            $this->db_handle->query($line) or die ('ERROR in ElectionRegistry::create_election_registry(): ' . $this->db_handle->error);
        }
    }
    
    private function clear_results() {
        while ($this->db_handle->more_results() && $this->db_handle->next_result()) {
            $extraResult = $this->db_handle->use_result();
    
            if($extraResult instanceof mysqli_result) {
                $extraResult->free();
            }
        }
    }
    
    function election_registry_exists(): bool {
        $query = 'SHOW DATABASES';
        $result = $this->db_handle->query($query) or die ('ERROR ' . $this->db_handle->error);
        
        foreach ($result as $row) {
            if ($row['Database'] == 'election_registry') {
                $result->free();
                return TRUE;
            }
        }
        
        $result->free();
        return FALSE;
    }
    
    function update_elections() {
        $election_database = NULL;
        $result = $this->db_handle->query('SELECT * FROM election_registry.created_elections') or die ('FAIL at ElectionRegistry::update_elections(): ' . $this->db_handle->error);
        
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $this->elections[$i] = new Election($row['tag']);
            $this->elections[$i]->set_started($row['started']);
            
            $election_database = new ElectionDatabase($this->elections[$i], $this->db_handle);
            $this->elections[$i]->set_candidates($election_database->get_candidates());
            
            $i++;
        }
        
        $result->free();
    }
    
    function update_registry() {
        $election_database = NULL;
        
        foreach ($this->elections as $election) {
            $election_database = new ElectionDatabase($election, $this->db_handle);
            $election_database->update_registry();
        }
    }
    
    function set_db_handle($db_handle) {
        $this->db_handle = $db_handle;
    }
    
    function get_db_handle() {
        return $this->db_handle;
    }
    
    function add_election(Election $election) {
        $this->elections[] = $election;
    }
    
    function set_elections(array $elections) {
        $this->elections = $elections;
    }
    
    function set_election(string $tag, Election $election) {
        for ($i = 0; $i < count($this->elections); $i++) {
            if ($this->elections[$i]->get_tag() == $tag) {
                $this->elections[$i] = $election;
                break;
            }
        }
    }
    
    function get_elections(): array {
        return $this->elections;
    }
    
    function get_election(string $tag): Election {
        foreach ($this->get_elections() as $election) {
            if ($election->get_tag() == $tag) {
                return $election;
            }
        }
    }
    
    function get_started_elections(): array {
        $started_elections = array();
        
        foreach ($this->get_elections() as $election) {
            if ($election->is_started()) {
                $started_elections[] = $election;
            }
        }
        
        return $started_elections;
    }
    
    function get_stopped_elections(): array {
        $stopped_elections = array();
        
        foreach ($this->get_elections() as $election) {
            if (!$election->is_started()) {
                $stopped_elections[] = $election;
            }
        }
        
        return $stopped_elections;
    }
}
?>
