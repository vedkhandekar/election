<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Candidate.php';

class Election {
    private $tag;
    private $started;
    private $candidates = array();
    
    function __construct(string $tag) {
        $this->set_tag($tag);
    }
    
    /*function __construct(string $tag, array $candidates) {
        $this->set_tag($tag);
        $thus->set_candidates($candidates);
    }*/
    
    function set_tag(string $tag) {
        $this->tag = $tag;
    }
    
    function get_tag(): string {
        return $this->tag;
    }
    
    function set_started(bool $started) {
        $this->started = $started;
    }
    
    function is_started(): bool {
        return $this->started;
    }
    
    function set_candidates(array $candidates) {
        $this->candidates = $candidates;
    }
    
    function get_candidates(): array {
        return $this->candidates;
    }
    
    /*static function compare_by_votes_desc(Candidate $c1, Candidate $c2): int {
        if ($c1->get_num_votes() == $c2->get_num_votes()) {
            return 0;
        } else if ($c1->get_num_votes() > $c2->get_num_votes()) {
            return -1;
        } else {
            return 1;
        }
    }*/
    
    
    // Returns an array of Candidate in desceding order of votes
    function get_candidates_desc(): array {
        $sorted_candidates = $this->get_candidates();
        usort($sorted_candidates, array("Candidate", "compare_by_votes_desc"));
        
        return $sorted_candidates;
    }
    
    function set_candidate(string $name, Candidate $candidate) {
        for ($i = 0; $i < count($this->candidates); $i++) {
            if ($this->candidates[$i]->get_name() == $name) {
                $this->candidates[$i] = $candidate;
            }
        }
    }
    
    function get_candidate(string $name): Candidate {
        foreach ($this->candidates as $candidate) {
            if ($candidate->get_name() == $name) {
                return $candidate;
            }
        }
    }
    
    function add_candidate(Candidate $candidate) {
        $this->candidates[] = $candidate;
    }
}

?>
