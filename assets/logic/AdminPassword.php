<?php

class AdminPassword {
    private $hash;
    private $db_handle;
    
    function __construct($db_handle) {
        $this->set_db_handle($db_handle);
        
        if (!$this->election_registry_exists()) {
            $this->create_election_registry();    
        }
        
        $this->update_hash();    
    }
    
    function create_election_registry() {
        $query = 'CREATE DATABASE election_registry;';
        $query .= 'CREATE TABLE election_registry.created_elections (tag VARCHAR(255) NOT NULL, started BOOLEAN NOT NULL);';
        $query .= 'CREATE TABLE election_registry.admin_passwd (passwd_hash VARCHAR(255) NOT NULL);';
        $query .= 'INSERT INTO election_registry.admin_passwd VALUES (\'$2y$10$/4DHRlUl2t2E13C9UlFDP.IqP8YKxcc.Z9DmV0en6FrowRsTO9EvO\')';
        
        
        foreach (explode(';', $query) as $line) {
            $this->db_handle->query($line) or die ('ERROR in AdminPassword::create_election_registry(): ' . $this->db_handle->error);
        }
    }
    
    function election_registry_exists(): bool {
        $query = 'SHOW DATABASES';
        $result = $this->db_handle->query($query) or die ('ERROR: ' . $this->db_handle->error);
        
        foreach ($result as $row) {
            if ($row['Database'] == 'election_registry') {
                $result->free();
                return TRUE;
            }
        }
        
        $result->free();
        return FALSE;
    }
    
    function set_db_handle($db_handle) {
        $this->db_handle = $db_handle;
    }
    
    function get_db_handle() {
        return $this->db_handle;
    }
    
    function set_hash($hash) {
        $this->hash = $hash;
    }
    
    function get_hash(): string {
        return $this->hash;
    }
    
    function update_hash() {
        //$row = $this->db_handle->query('SELECT passwd_hash FROM election_registry.admin_passwd')->fetch_assoc();
        $result = $this->db_handle->query('SELECT passwd_hash FROM election_registry.admin_passwd') or die ('FAIL at AdminPassword::update_hash(): ' . $this->db_handle->error);
        $row = $result->fetch_assoc();
        $this->set_hash($row['passwd_hash']);
    }
    
    function write_hash($new_hash): bool {
        $query = sprintf('UPDATE election_registry.admin_passwd SET passwd_hash=\'%s\'', $new_hash);
        return $this->db_handle->query($query);
    }
}

?>
