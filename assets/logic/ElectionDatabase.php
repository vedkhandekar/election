<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Election.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Candidate.php';

class ElectionDatabase {
    private $election;
    private $db_handle;
    
    function __construct($election, $db_handle) {
        $this->set_election($election);
        $this->set_db_handle($db_handle);
    }
    
    function set_election(Election $election) {
        $this->election = $election;
    }
    
    function get_election(): Election {
        return $this->election;
    }
    
    function set_db_handle($db_handle) {
        $this->db_handle = $db_handle;
    }
    
    function get_db_handle(): Database {
        return $this->db_handle;
    }
    
    function get_database_name(): string {
        return $this->election->get_tag() . '_db';
    }
    
    function get_poll_table_name(): string {
        return $this->election->get_tag() . '_polls';
    }
    
    function get_voting_table_name(): string {
        return $this->election->get_tag() . '_voting';
    }
    
    function get_voterid_table_name(): string {
        return $this->election->get_tag() . '_voterid';
    }
    
    function create_database(): bool {
        // accessed directly for convinience
        return $this->db_handle->query('CREATE DATABASE ' . $this->get_database_name());
    }
    
    function use_database(): bool {
        return $this->db_handle->query('USE ' . $this->get_database_name());
    }
    
    function create_tables(): bool {
        return $this->create_poll_table() && $this->create_voting_table() && $this->create_voterid_table();
    }
    
    // Poll Table is the one in which the number of votes are stored
    function create_poll_table(): bool {
        $query = 'CREATE TABLE ' . $this->get_poll_table_name() . ' (candidate_name VARCHAR(255) UNIQUE NOT NULL, num_votes SMALLINT UNSIGNED NOT NULL DEFAULT 0)';
        return $this->db_handle->query($query);
    }
    
    // Voting Table consists of name of the candidate and the path her/his symbol
    function create_voting_table(): bool {
        $query = 'CREATE TABLE ' . $this->get_voting_table_name() . ' (candidate_name VARCHAR(255) UNIQUE NOT NULL, symbolpath VARCHAR(1024) NOT NULL)';
        return $this->db_handle->query($query);
    }
    
    function create_voterid_table(): bool {
        $query = sprintf('CREATE TABLE %s (voter_name VARCHAR(255) UNIQUE NOT NULL)', $this->get_voterid_table_name());
        return $this->db_handle->query($query);
    }
    
    // Perhaps a inappropiate title, this function writes $this->election to the database
    function dump_election_candidates() {
        $query = '';
        
        foreach ($this->election->get_candidates() as $candidate) {
            $query = sprintf('INSERT INTO %s (candidate_name) VALUES (\'%s\')', $this->get_poll_table_name(), $candidate->get_name());
            $this->db_handle->query($query);
            
            $query = sprintf('INSERT INTO %s VALUES (\'%s\', \'%s\')', $this->get_voting_table_name(), $candidate->get_name(), $candidate->get_symbol_path());
            $this->db_handle->query($query);
        }
        
        $this->db_handle->commit();
    }
    
    function update_registry(): bool {
        $query = sprintf('SELECT tag FROM election_registry.created_elections WHERE tag=\'%s\'', $this->election->get_tag());
        $result = $this->db_handle->query($query) or die ('FAIL 1: ' . $this->db_handle->error);
        $result_count = count($result->fetch_assoc());
        
        $ret_val = FALSE;
        
        $raw_started = $this->election->is_started() ? 1 : 0;
        if ($result_count == 1) {
            $query = sprintf('UPDATE election_registry.created_elections SET started=%d WHERE tag=\'%s\'', $raw_started, $this->election->get_tag());
            $ret_val = $this->db_handle->query($query) or die ('FAIL 2: ' . $this->db_handle->error);
        } else {
            $result->free();
            $query = sprintf('INSERT INTO election_registry.created_elections VALUES (\'%s\', %d)', $this->election->get_tag(), $raw_started);
            $ret_val = $this->db_handle->query($query) or die ('FAIL 3: ' . $this->db_handle->error);;
        }
        
        $result->free();
        $this->db_handle->commit();
        
        return $ret_val;
    }
    
    function update_poll_table() {
        foreach ($this->election->get_candidates() as $candidate) {
            $query = sprintf('UPDATE %s.%s SET num_votes=%d WHERE candidate_name=\'%s\'', $this->get_database_name(), $this->get_poll_table_name(), $candidate->get_num_votes(), $candidate->get_name());
            $this->db_handle->query($query) or die ($this->db_handle->error);
        }
    }
    
    function get_candidates(): array {
        $candidates = array();
        
        $query = sprintf('SELECT * FROM %s.%s', $this->get_database_name(), $this->get_voting_table_name());
        $result = $this->db_handle->query($query);
        
        $i = 0;
        while  ($row = $result->fetch_assoc()) {
            $candidates[$i] = new Candidate($row['candidate_name'], $row['symbolpath']);
            
            $query = sprintf('SELECT num_votes FROM %s.%s WHERE candidate_name=\'%s\'', $this->get_database_name(), $this->get_poll_table_name(), $candidates[$i]->get_name());
            $nvotes_result = $this->db_handle->query($query)->fetch_assoc();
            $candidates[$i]->set_num_votes($nvotes_result['num_votes']);
            
            $i++;
        }
        
        $result->free();
        
        return $candidates;
    }
    
    // Checks if a voter with name $voter_name exists in the _voterid table  
    function has_voted($voter_name): bool {
        $query = sprintf('SELECT voter_name FROM %s.%s WHERE voter_name=\'%s\'', $this->get_database_name(), $this->get_voterid_table_name(), $voter_name);
        $num_results = count($this->db_handle->query($query)->fetch_assoc());
        
        return $num_results == 1 ? TRUE : FALSE;
    }
    
    // Adds a voter with name $voter_name to the _voterid table
    function add_voter($voter_name) {
        $query = sprintf('INSERT INTO %s.%s VALUES (\'%s\')', $this->get_database_name(), $this->get_voterid_table_name(), $voter_name);
        $this->db_handle->query($query) or die($this->db_handle->error);
    }
}
?>
