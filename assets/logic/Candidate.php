<?php

class Candidate {
    private $name;
    private $symbol_path;
    private $num_votes = 0;
    
    function __construct($name, $symbol_path) {
        $this->set_name($name);
        $this->set_symbol_path($symbol_path);
        $this->set_num_votes(0);
    }
    
    function set_name($name) {
        $this->name = $name;
    }
    
    function get_name() {
        return $this->name;
    }
    
    function get_codified_name(): string {
        return str_replace(' ', '_', strtolower($this->name));
    }
    
    function set_symbol_path($symbol_path) {
        $this->symbol_path = $symbol_path;
    }
    
    function get_symbol_path() {
        return $this->symbol_path;
    }
    
    function set_num_votes(int $num_votes) {
        $this->num_votes = $num_votes;
    }
    
    function get_num_votes() {
        return $this->num_votes;
    }
    
    function increment_num_votes() {
        $this->set_num_votes($this->get_num_votes() + 1);
    }
    
    static function compare_by_votes_desc(Candidate $c1, Candidate $c2): int {
        if ($c1->get_num_votes() == $c2->get_num_votes()) {
            return 0;
        } else if ($c1->get_num_votes() > $c2->get_num_votes()) {
            return -1;
        } else {
            return 1;
        }
    }
}

?>
