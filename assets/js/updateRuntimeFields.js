var runtime_fields = document.getElementById('runtime_fields');
updateRuntimeFields();

/*
<tr>
                        <td colspan="3">
                            <table border="0">
                                <tr>
                                    <td style="width: 5%"></td>
                                    <td colspan="3"> Candidate 1:</td>
                                </tr>
                                
                                <tr>
                                    <td></td>
                                    
                                    <td style="width: 5%"></td>
                                    
                                    <td>Name:</td>
                                    
                                    <td>
                                        <input type="text" name="candidate1_name" required />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td></td>
                                    
                                    <td></td>
                                    
                                    <td>Symbol:</td>
                                    
                                    <td><input type="file" name="candidate1_symbolpath" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
 */

function updateRuntimeFields() {
    var num_candidates = document.getElementById('num_candidates').value; 
    var i;

    runtime_fields.innerHTML = ''
        
    for (i = 1; i <= num_candidates; i++) {
        console.log('Candidate ' + i);
        
        runtime_fields.innerHTML += ['<tr>',
        '<td colspan=\"3\">',
        '<table border=\"0\">',
        '<tr>',
        '<td style=\"width: 5%\"></td>',
        '<td colspan=\"3\"> Candidate ' + i + ':</td>',
        '</tr>',
        '<tr>',
        '<td></td>',
        '<td style=\"width: 5%\"></td>',
        '<td>Name:</td>',
        '<td>',
        '<input type=\"text\" name=\"candidate' + i + '_name\" required />',
        '</td>',
        '</tr>',
        '<tr>',
        '<td></td>',
        '<td></td>',
        '<td>Symbol:</td>',
        '<td><input type=\"file\" name=\"candidate' + i + '_symbolpath\" /></td>',
        '</tr>',
        '</table>',
        '</td>',
        '</tr>'].join('');
    }
}
