<!DOCTYPE html>

<?php
session_start();
$_SESSION['logged_in'] = FALSE;

$redirect_location = 'admin.php';
$redirect_script = '';
$passwd_inc = FALSE;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require 'web_magic/webm_admin.php';
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Admin Login</title>
    
    <style>
    .error {
        color: red;
    }
    </style>
    
    <script>
    <?php
    echo $redirect_script;
    ?>
    </script>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2>Admin Login</h2>
    </center>
    
    <hr/>
    
    <table style="width:100%;height:300px" border="0">
        <tr style="height:33%">
            <td style="width:33%"></td>
            <td style="width:34%"></td>
            <td style="width:33%"></td>
        </tr>
        
        <tr style="height:34%">
            <td></td>
            <td>
                <?php
                if ($passwd_inc) {
                    echo '<center><b class="error">Password is incorrect</b></center>';
                }
                ?>
                <form id="passwd_form" method="POST" action="<?php echo $redirect_location ?>">
                <table style="width: 100%; height: 100%; text-align: center" border="0">
                    <tr>
                        <td style="width:50%">
                            Password:
                        </td>
                        
                        <td style="width:50%">
                            <input type="password" size="10" name="admin_passwd" required />
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2"><br/><br/></td>
                    </tr>
                    
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="Login" />
                        </td>
                    </tr>
                </table>
            </td>
            <td></td>
        </tr>
        
        <tr style="height:33%">
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    
    <hr/>
    <center>
        &copy; <?php echo date('Y'); ?> Ved Khandekar
    </center>
    
    <script>
    <?php
    echo $redirect_script;
    ?>
    </script>
</body>
</head>
</html>
