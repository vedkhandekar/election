<!DOCTYPE html>

<?php
require $_SERVER['DOCUMENT_ROOT'] . '/web_magic/webm_admin_tools/webm_clear_assets.php';
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Clear Assets</title>
    
    <script>
    function check_all() {
        var num_files = document.getElementById('num_files').value;
        
        for(i = 0; i < num_files; i++) {
            document.getElementById('file' + i).checked = true;
        }
    }
    </script>
</head>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2>Clear <code>/assets/candidate_symbols/</code></h2>
    </center>
    
    <hr/>
    
    <table style="width: 100%">
        <tr>
            <td style="width: 2%"></td>
            
            <!-- CONTENT -->
            <td style="width: 96%">
                <p style="text-align: right"><a href="/admin_tools/admin_dboard.php">&larr; Back</a></p>
                
                Contents of <code>/assets/candidate_symbols/</code>:
                <form method="POST" action="/admin_tools/clear_assets.php">
                <table border="0">
                <?php
                if (count($file_names) == 0) {
                ?>
                    <tr>
                        <td width="15"></td>
                        <td colspan="2"><i>The directory is empty</i></td>
                    </tr>
                <?php
                } else {
                    $i = 0;
                    foreach ($file_names as $file_name) {
                ?>
                        <tr>
                            <td width="15"></td>
                            
                            <td style="text-align: center">
                                <input type="checkbox" id="file<?php echo $i; ?>" name="file<?php echo $i; ?>" value="<?php echo $file_name; ?>" />
                            </td>
                            
                            <td><?php echo $file_name; ?></td>
                        </tr>
                <?php
                        $i++;
                    }
                ?>
                <tr>
                        <td colspan="3">
                            <br/>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="3" style="text-align: center">
                            <input type="button" onclick="check_all()" value="Select All Files" />
                            <input type="submit" value="Delete Files" />
                        </td>
                    </tr>
                <?php
                }
                ?>
                </table>
                
                </form>
                <input type="hidden" id="num_files" value="<?php echo count($file_names); ?>" />
            </td>
            
            <td style="width: 2%"></td>
        </tr>
    </table>
</body>
</html>
