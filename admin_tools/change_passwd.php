<!DOCTYPE html>

<?php
$msg = '';
require $_SERVER['DOCUMENT_ROOT'] . '/web_magic/webm_admin_tools/webm_change_passwd.php';
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Change Password</title>
</head>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2>Change Admin Password</h2>
    </center>

    <hr/>
        
    <table width="100%">
        <tr>
            <td style="width: 2%"></td>
            
                
            <!-- CONTENT -->
            <td style="width: 96%">
                <p style="text-align: right"><a href="/admin_tools/admin_dboard.php">&larr; Back</a></p>
                
                <form method="POST" action="/admin_tools/change_passwd.php">
                <?php
                if ($_SERVER['REQUEST_METHOD'] == 'POST')
                echo '<b>' . $msg . '</b>';
                ?>
                <table>
                    <tr>
                        <td>Old Password:</td>
                        
                         <td>
                            <input type="password" name="old_passwd" required />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>New Password:</td>
                        
                        <td>
                            <input type="password" name="new_passwd" required />
                        </td>
                    </tr>
                    
                    <tr>
                       <td>Confirm New Password:</td>
                        
                        <td>
                            <input type="password" name="cnf_new_passwd" required />
                        </td>
                    </tr>
                    
                    <tr><td colspan="2"><br/></td></tr>
                    
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <input type="submit" value="Change Password" />
                        </td>
                    </tr>
                </table>
                </form>
            </td>
            
            <td style="width: 2%"></td>
        </tr>
    </table>
</body>
</html>
