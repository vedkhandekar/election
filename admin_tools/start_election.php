<!DOCTYPE html>

<?php
$started_elections_exist = FALSE;
$all_elections_active = FALSE;
require $_SERVER['DOCUMENT_ROOT'] . '/web_magic/webm_admin_tools/webm_start_election.php';
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Start Election</title>
</head>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2>Start Election</h2>
    </center>
    
    <hr/>
    
    <table style="width: 100%" border="0">
        <tr>
            <td style="width: 2%"></td>
            
            <td style="width: 96%">
                <p align="right"><a href="/admin_tools/admin_dboard.php">&larr; Back</a></p>
                
                Currently active elections:
                <ul>
                <?php
                if ($started_elections_exist) {
                    foreach ($started_elections as $election) {
                    ?>
                    <li><?php echo $election->get_tag(); ?></li>
                    <?php
                    }
                } else {
                ?>
                <li><i>None</i></li>
                <?php
                }
                ?>
                </ul>
                
                <br/><br/>
                
                <b>Please select an election to start:</b>
                <form method="POST" action="/admin_tools/start_election.php">
                <table border="0">
                    <tr>
                        <td>Start Election:</td>
                        
                        <td>
                            <?php
                            if ($all_elections_active) {
                            ?>
                            <i>All created elections have been started</i>
                            <?php
                            } else {
                            ?>
                            <select name="admin_choice">
                                <?php
                                foreach ($stopped_elections as $election) {
                                ?>
                                <option value="<?php echo $election->get_tag(); ?>"><?php echo $election->get_tag(); ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <?php 
                            } 
                            ?>
                        </td>
                    </tr>
                    
                    <tr><td colspan="2"><br/></td></tr>
                    
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <input type="submit" value="Start Election" <?php if ($all_elections_active) echo 'disabled'; ?> />
                        </td>
                    </tr>
                </table>
                </form>
            </td>
            
            <td style="width: 2%"></td>
        </tr>
    </table>
</body>
</html>
