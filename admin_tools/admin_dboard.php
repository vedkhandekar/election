<!DOCTYPE html>

<?php
require $_SERVER['DOCUMENT_ROOT'] . '/web_magic/webm_admin_tools/webm_admin_dboard.php';
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Admin Dashboard</title>
</head>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2>Admin's Dashboard</h2>
    </center>
    
    <hr/>
    
    <table style="width: 100%" border="0">
        <tr>
            <td style="width: 2%"></td>
            
            <td style="width: 96%">
                <p align="right"><i><a href="/admin.php">Logout</a></i></p>
                Welcome Admin, here is all what you can do:
                <ul>
                    <li>
                        <a href="create_election.php">Create a new election</a>
                    </li>
                    
                    <li>
                        <a href="start_election.php">Start an existing election</a>
                    </li>
                    
                    <li>
                        <a href="stop_election.php">Stop an ongoing election</a>
                    </li>
                    
                    <li>
                        <a href="view_results.php">View Results</a>
                    </li>
                </ul>
                
                But you can also,
                <ul>
                    <li>
                        <a href="change_passwd.php">Change Admin Password</a>
                    </li>
                    
                    <li>
                        <a href="clear_assets.php">Selectively Clear <code>/assets/candidate_symbols/</code></a>
                    </li>
                </ul>
            </td>
            
            <td style="width: 2%"></td>
        </tr>
    </table>
</body>    
</html>
