<!DOCTYPE html>

<?php
require $_SERVER['DOCUMENT_ROOT'] . '/web_magic/webm_admin_tools/webm_view_results.php';
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>View Results</title>
</head>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2>View Results</h2>
    </center>
    
    <hr/>
    
    <table style="width: 100%">
        <tr>
            <td style="width: 2%"></td>
            
            <!--  CONTENT -->
            <td style="width: 96%">
                <p style="text-align: right"><a href="/admin_tools/admin_dboard.php">&larr; Back</a></p>
                
                <form method="POST" action="/admin_tools/view_results.php">
                <table>
                    <tr>
                        <td>View result of:</td>
                        
                        <td>
                            <?php
                            if (count($stopped_elections) == 0) {
                            ?>
                            <i>There are no stopped elections to view results of</i>
                            <?php
                            } else {
                            ?>
                            <select name="admin_election_choice">
                                <?php
                                foreach ($stopped_elections as $election) {
                                ?>
                                    <option value="<?php echo $election->get_tag(); ?>"
                                    <?php
                                    if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_SESSION['logged_in']) {
                                        if ($election->get_tag() == $admin_election_choice) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>
                                    >
                                        <?php echo $election->get_tag(); ?>
                                    </option>
                                <?php
                                }
                                ?>
                            </select>
                            <?php
                            }
                            ?>
                        </td>
                        
                        <?php
                        if (count($stopped_elections) != 0) {
                        ?>
                        <td>
                            <input type="submit" value="View Results" />
                        </td>
                        <?php
                        }
                        ?>
                    </tr>
                </table>
                </form>
                
                <br/>
                
                <?php
                if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_SESSION['logged_in']) {
                ?>
                    <table cellpadding="10" style="width: 50%; text-align: center" border="1">
                        <tr style="background-color: darkblue; color: white">
                            <th>Sr. No.</th>
                            <th>Candidate Name</th>
                            <th>% Votes</th>
                            <th>Votes</th>
                        </tr>
                        
                        <?php
                        $i = 1;
                        foreach ($candidates as $candidate) {
                        ?>
                            <tr
                            <?php
                            if ($i == 1) {
                                echo 'style="background-color: gold"';
                            }
                            
                            if ($i == 2) {
                                echo 'style="background-color: silver"';
                            }
                            ?>
                            >
                                <td><?php echo $i; ?>.</td>
                                
                                <td><?php echo $candidate->get_name(); ?></td>
                                
                                <td><?php echo number_format(($candidate->get_num_votes() / $total_votes) * 100, 2, '.', ''); ?>%</td>
                                
                                <td><?php echo $candidate->get_num_votes(); ?></td>
                            </tr>
                        <?php
                        $i++;
                        }
                        ?>
                        
                        <tr>
                            <td colspan="2"><b><i>Total</i></b></td>
                            <td><b><i>100%</i></b></td>
                            <td><b><i><?php echo $total_votes; ?></i></b></td>
                        </tr>
                    </table>
                <?php
                }
                ?>
            </td>
            
            <td style="width: 2%"></td>
        </tr>
    </table>
</body>
</html>
