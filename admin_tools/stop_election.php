<!DOCTYPE html>

<?php
$stopped_elections_exist = FALSE;
require $_SERVER['DOCUMENT_ROOT'] . '/web_magic/webm_admin_tools/webm_stop_election.php';
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Stop Election</title>
</head>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2>Stop Election</h2>
    </center>
    
    <hr/>
    
    <table style="width: 100%">
        <tr>
            <td style="width: 2%"></td>
            
            <!-- CONTENT -->
            <td style="width: 96%">
                <p style="text-align: right"><a href="/admin_tools/admin_dboard.php">&larr; Back</a></p>
                Currently stopped elections:
                <ul>
                <?php
                if (!$stopped_elections_exist) {
                ?>
                <li><i>None</i></li>
                <?php
                } else {
                    foreach ($stopped_elections as $election) {
                ?>
                    <li><?php echo $election->get_tag(); ?></li>
                <?php
                    }
                }
                ?>
                </ul>
            
                <br/><br/>
            
                <b>Please select an election to stop:</b>
                <form method="POST" action="/admin_tools/stop_election.php">
                <table>
                    <tr>
                        <td>Stop Election:</td>
                        
                        <td>
                        <?php
                        if (count($started_elections) > 0) {
                        ?>
                        <select name="admin_choice">
                            <?php
                            foreach ($started_elections as $election) {
                            ?>
                            <option value="<?php echo $election->get_tag(); ?>"><?php echo $election->get_tag(); ?></option>
                            <?php
                            }
                            ?>
                        </select>
                        <?php
                        } else {
                        ?>
                        <i>All created elections are stopped</i>
                        <?php
                        }
                        ?>
                        </td>
                    </tr>
                    
                    <tr><td colspan="2"><br/></td></tr>
                    
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <input type="submit" value="Stop Election" <?php if (count($started_elections) == 0) echo 'disabled'; ?> />
                        </td>
                    </tr>
                </table>
                </form>
            </td>
            
            <td style="width: 2%"></td>
        </tr>
    </table>
</body>
</html>
