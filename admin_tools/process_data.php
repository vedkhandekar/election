<?php
require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Candidate.php';
require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Election.php';
require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionDatabase.php';
require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';

define ('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'] . '/assets/candidate_symbols/');

session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_SESSION['logged_in']) {
    $election = new Election(htmlspecialchars($_POST['tag']));
    $election->set_started(FALSE);
    
    $num_candidates = (int) $_POST['num_candidates'];
    
    // TODO: modularize this:
    for ($i = 1; $i <= $num_candidates; $i += 1) {
        $name_index = 'candidate' . $i . '_name';
        $symbolpath_index = 'candidate' . $i . '_symbolpath';
        
        if ($_FILES[$symbolpath_index]['error'] == UPLOAD_ERR_OK) {
            $tmp_name = $_FILES[$symbolpath_index]['tmp_name'];
            $actual_name = basename($_FILES[$symbolpath_index]['name']);
            
            move_uploaded_file($tmp_name, UPLOAD_DIR . $actual_name);
            $election->add_candidate(new Candidate($_POST[$name_index], UPLOAD_DIR . $actual_name));
        }
    }
    
    $db_handle = new mysqli('172.17.0.2', 'root', 'root'); //TODO: change this while making Dockerfile
    
    //TODO: probably wrap all this in one function
    $election_db = new ElectionDatabase($election, $db_handle); 
    $election_db->create_database();
    $election_db->use_database();
    $election_db->create_tables();
    $election_db->dump_election_candidates();
    
    $election_registry = new ElectionRegistry($db_handle);
    $election_registry->add_election($election);
    $election_registry->update_registry();
    $db_handle->close();
    
    header('Location: /admin_tools/admin_dboard.php');
}
?>
