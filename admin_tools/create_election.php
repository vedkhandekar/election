<!DOCTYPE html>

<?php
require $_SERVER['DOCUMENT_ROOT'] . '/web_magic/webm_admin_tools/webm_create_election.php';
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Create Election</title>
</head>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2>Create Election</h2>
    </center>
    
    <hr/>
    
    <table style="width: 100%" border="0">
        <tr>
            <td style="width: 2%"></td>
            <td style="width: 96%"></td>
            <td style="width: 2%"></td>
        </tr>
        
        <tr>
            <td></td>
            
            <!-- CONTENT -->
            <td>
                <p align="right"><a href="/admin_tools/admin_dboard.php">&larr; Back</a></p>
                <b><u>Please fill the following information to create an Election:</u></b><br/><br/>
                <form enctype="multipart/form-data" name="create_election_form" method="POST" action="process_data.php">
                <table border="0">
                    <tr>
                        <td>Tag:</td>
                        
                        <td>
                            <input type="text" placeholder="e.g. headgirl_elections" name="tag" required />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Number of Candidates:</td>
                        
                        <td>
                            <input type="number" value="3" min="2" name="num_candidates" id="num_candidates" onchange="updateRuntimeFields()" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="3"><br/><br/></td>
                    </tr>
                    
                    <tr>
                        <td colspan="3">Candidates:</td>
                    </tr>
                    
                    
                    
                    <tbody id="runtime_fields">
                    </tbody>
                    <!-- end -->
                    
                    <tr><td colspan="3"><br/><br/></td></tr>
                    
                    <tr>
                        <td colspan="3" style="text-align: center">
                            <input type="submit" value="Create Election" />
                        </td>
                    </tr>
                </table>
                </form>
            </td>
            
            <td></td>
        </tr>
        
        <tr>
        </tr>
    </table>
    
    <script src="/assets/js/updateRuntimeFields.js"></script>
</body>
</html>
