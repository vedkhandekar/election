<!DOCTYPE html>

<?php
$js_code = '';
$action_location = 'index.php';

require $_SERVER['DOCUMENT_ROOT'] . '/web_magic/webm_index.php';
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>ELECTION</title>
    
    <style>
    u {
        color: blue;
    }
    </style>
</head>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2><u>E</u>lectronically <u>L</u>et '<u>E</u>m <u>C</u>hoose <u>T</u>heir <u>I</u>nteresting and <u>O</u>utstanding <u>N</u>ominees</h2>
    </center>
    
    <hr/>
    
    <table style="width: 100%; height: 350px" border="0">
        <tr>
            <td style="width: 33%; height: 33%"></td>
            <td style="width: 34%"></td>
            <td style="width: 33%"></td>
        </tr>
    
        <tr>
            <td style="height: 34%">
            </td>
            
            <!-- CONTENT -->
            <td>
                <form method="POST" action="/<?php echo $action_location; ?>">
                <table style="width: 100%" border="0">
                    <tr>
                        <td>Election:</td>
                        
                        <td>
                            <?php
                            if ($started_elections_exist) {
                            ?>
                            <select name="voter_election_choice">
                                <?php
                                foreach ($started_elections as $election) {
                                ?>
                                    <option value="<?php echo $election->get_tag(); ?>"
                                    <?php
                                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                                        if ($election->get_tag() == $voter_election_choice) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>
                                    >
                                        <?php echo $election->get_tag(); ?>
                                    </option>
                                <?php
                                }
                                ?>
                            </select>
                            <?php
                            } else {
                            ?>
                            <i>There are no started elections</i>
                            <?php
                            }
                            ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Name:</td>
                        
                        <td>
                            <input type="text" placeholder="Full name in capitals" oninput="capitalize()" id="voter_name" name="voter_name"
                            <?php
                            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                                echo sprintf('value="%s"', $voter_name);
                            }
                            ?>
                            required />
                            <sup><b>*</b></sup>
                        </td>
                    </tr>
                    
                    <tr><td colspan="2"><br/></td></tr>
                    
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <input type="submit" value="Next"
                            <?php
                            if (!$started_elections_exist) {
                                echo 'disabled';
                            }
                            ?> />
                        </td>
                    </tr>
                </table>
                </form>
            </td>
            
            <td></td>
        </tr>
    
        <tr>
            <td style="height: 33%">
            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <h6>* We won't register your vote with your name, <i>i.e.</i> we keep your vote secret.</h6>
    <hr/>
    
    <table style="width: 100%" border="0">
        <tr>
            <td style="width: 50%; vertical-align: middle">
                <img src="/assets/site_images/php-power-white.png" width="88" height="33" />
            </td>
            
            <td style="width: 50%; text-align: right; vertical-align: middle">
                <h5><i>Brought to you by Ved Khandekar (<a href="mailto:vedk90@gmail.com">vedk90@gmail.com</a>)</i></h5>
            </td>
        </tr>
    </table>
    
    <script>
    function capitalize() {
        document.getElementById('voter_name').value = document.getElementById('voter_name').value.toUpperCase();
    }
    </script>
    <script>
    <?php echo $js_code; ?>
    </script>
</body>
</html>
