<!DOCTYPE html>

<?php
require $_SERVER['DOCUMENT_ROOT'] . '/web_magic/webm_vote_screen.php';
?>

<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Vote Screen</title>
</head>
<body>
    <center>
        <h1>ELECTION</h1>
        <h2>Vote Screen</h2>
    </center>
    
    <hr/>
    
    <table style="width: 100%" border="0">
        <tr>
            <td style="width: 2%">
        
            <td style="width: 96%">
                <center>
                    <form method="POST" action="cast_vote.php">
                    
                    <!-- A hidden field to store the election the voter is voting in -->
                    <input type="hidden" name="voter_election_choice" value="<?php echo $_POST['voter_election_choice']; ?>" />
                    <input type="hidden" name="voter_candidate_choice" id="voter_candidate_choice" />
                    
                    <table style="width: 50%; text-align: center" border="1">
                        <caption>
                            <b>Click</b> on the <b>symbol</b> of the candidate to vote her/him<br/>
                            Please do also <b>scroll down</b> to view other candidates<br/>
                            There are <b><?php echo count($candidates); ?> candidates</b> in this election
                        </caption>
                        
                        <tr>
                            <th style="width: 4%">Sr. No.</th>
                            <th style="width: 28%">Symbol</th>
                            <th style="width: 68%">Name</th>
                        </tr>
                        
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($candidates as $candidate) {
                            ?>
                                <tr>
                                    <td><?php echo $i; ?>.</td>
                                    <td>
                                        <input type="image" id="<?php echo $candidate->get_codified_name(); ?>_button" onclick="<?php echo $candidate->get_codified_name(); ?>_button_clicked()" width="100" height="100" src="<?php echo $symbolpath_r[$candidate->get_name()]; ?>" />
                                    </td>
                                    
                                    <td><?php echo $candidate->get_name(); ?></td>
                                </tr>
                            <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                        
                        <tr>
                            <td colspan="3"><br/></td>
                        </tr>
                        
                        <tr>
                            <td colspan="3"><i>END OF THE LIST OF CANDIDATES</i></td>
                        </tr>
                    </table>
                    </form>
                </center>
            </td>
            
            <td style="width: 2%"></td>
        </tr>
    </table>
    
    <script>
    <?php
    foreach ($candidates  as $candidate) {
    ?>
        function <?php echo $candidate->get_codified_name(); ?>_button_clicked() {
            document.getElementById('voter_candidate_choice').value = '<?php echo $candidate->get_name(); ?>';
        }
    <?php
    }
    ?>
    </script>    
</body>
</html>
