<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/Candidate.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionDatabase.php';
    require $_SERVER['DOCUMENT_ROOT'] . '/assets/logic/ElectionRegistry.php';
    
    $voter_election_choice = $_POST['voter_election_choice'];
    $voter_candidate_choice = $_POST['voter_candidate_choice'];
    
    $db_handle = new mysqli('172.17.0.2', 'root', 'root'); // TODO: change this while writing DOckerfile
    
    $election_registry = new ElectionRegistry($db_handle);
    $election = $election_registry->get_election($voter_election_choice);
    $candidate = $election->get_candidate($voter_candidate_choice);
    $candidate->increment_num_votes();
    $election->set_candidate($voter_candidate_choice, $candidate);
    $election_database = new ElectionDatabase($election, $db_handle);
    $election_database->update_poll_table();
    
    $db_handle->close();
    
    echo 'Vote cast successfully. Redirecting you to the <a href="/index.php">main page</a> in 3 seconds';
    header('refresh: 3; url=/index.php');
    
} else {
    header('Location: /index.php');
}

?>
